import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HelperService } from '../services/helper.service';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {
  notesData: any = [];
  notesForm: FormGroup;
  modalRef: BsModalRef;
  editId: any;
  constructor(
    private home: HomeService,
    private modalService: BsModalService,
    public helper: HelperService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.getNotes();
    this.notesForm = this.fb.group({
      title: [''],
      description: [''],
    });
  }

  ngOnInit(): void {
  }

  getNotes() {
    this.home.getNotes().subscribe(
      (res: any) => {
        console.log(res);
        this.notesData = res.notesData;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addNote() {
    const data: any = {};
    data.title = this.notesForm.value.title;
    data.description = this.notesForm.value.description;
    if (!this.editId) {
      this.home.addNote(data).subscribe(
        (res) => {
          this.helper.showSuccess('Note Added Successfully.');
          this.getNotes();
          this.modalRef.hide();
        },
        (err) => console.log(err)
      );
    } else {
      data.id = this.editId;
      this.home.updateNote(data).subscribe(
        (res) => {
          this.helper.showSuccess('Note updated successfully');
          this.getNotes();
          this.modalRef.hide();
        },
        (err) => console.log(err)
      );
    }
  }

  deleteNote(id) {
    const data: any = {};
    data.id = id;
    this.home.deleteNote(data).subscribe((res) => {
      this.helper.showSuccess('Note deleted successfully');
      this.getNotes();
    }, err => console.log(err));
  }

  logout() {
    this.helper.delAllPREF();
    this.router.navigate(['/login']);
  }

  openModal(template: TemplateRef<any>, id?, index?) {
    this.editId = id ? id : null;
    if (this.editId) {
      this.notesForm.get('title').setValue(this.notesData[index].title);
      this.notesForm.get('description').setValue(this.notesData[index].description);
    }
    this.modalRef = this.modalService.show(template);
  }
  closeModel() {
    this.editId = null;
    this.modalRef.hide();
  }
}
