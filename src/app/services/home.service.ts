import { Injectable } from '@angular/core';
import { ApisService } from './apis.service';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  token: any;
  constructor(public apiService: ApisService, private helper: HelperService) { }

  userSignup(data) {
    return this.apiService.postWithoutHeader('/user/sign-up', data);
  }

  userLogin(data) {
    return this.apiService.postWithoutHeader('/user/login', data);
  }

  addNote(data) {
    return this.apiService.postWithHeader('/notes/add-notes', data, this.helper.getPREF('token'));
  }

  getNotes() {
    console.log(this.helper.getPREF('token'))
    return this.apiService.getWithHeader('/notes/get-notes', this.helper.getPREF('token'));
  }

  updateNote(data) {
    return this.apiService.postWithHeader('/notes/update-notes/' + data.id, data ,this.helper.getPREF('token'))
  }

  deleteNote(data) {
    return this.apiService.deleteWithHeader('/notes/delete-notes/' + data.id, this.helper.getPREF('token'))
  }
}
